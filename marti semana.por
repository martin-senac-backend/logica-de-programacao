programa
{
	
	funcao inicio()
	{
		inteiro diasemana

		escreva("Digite um Valor de 1 a 7: ")
		leia(diasemana)

		se(diasemana == 1){
			escreva("Domingo")
		}senao se(diasemana == 2){
			escreva("Segunda")
		}senao se(diasemana == 3){
			escreva("Terça")
		}senao se(diasemana == 4){
			escreva("Quarta")
		}senao se(diasemana == 5){
			escreva("Quinta")
		}senao se(diasemana == 6){
			escreva("Sexta")
		}senao se(diasemana == 7){
			escreva("Sabado")
		}senao{
			escreva("Numero Invalido")
		}

		// Utilizando a Condição de Escolha "Switch Case"

		escreva("\n\n")

		escolha(diasemana){
			caso 1: 
				escreva("Domingo")
				pare
			caso 2:
				escreva("Segunda")
				pare
			caso 3:
				escreva("Terça")
				pare
			caso 4:
				escreva("Quarta")
				pare
			caso 5:
				escreva("Quinta")
				pare
			caso 6:
				escreva("Sexta")
				pare
			caso 7:
				escreva("Sabado")
				pare

			caso contrario:
				escreva("Valor Invalido")


			
			}
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 505; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */