programa
{
	
	funcao inicio()
	{
		inteiro num  

		escreva("Digite o valor: ")
		leia(num) 

		se(num%2 == 0){
			escreva("O número ", num, " é divisivel por 2.") 
		}
		se(num%3 == 0){
			escreva("\nO número ", num, " é divisivel  por 3.") 
		}
	}
	
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 206; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */